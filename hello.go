package main

import (
	"fmt"
	"os"
	"strings"
)

func main() {
	fmt.Println("hello world")
	fmt.Println("pid:", os.Getpid())
	path, _ := os.Executable()
	fmt.Println("path:", path)
	parent, _ := os.Open(".")
	files, _ := parent.Readdirnames(0)
	fmt.Println("current dir files:", strings.Join(files, ", "))
}
