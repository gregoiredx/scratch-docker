.PHONY: hello hello-docker clean

build:
	go build hello.go

run:
	./hello

build-docker:
	docker build --tag hello .

run-docker:
	docker run hello

clean:
	rm -f hello
	docker rm $(shell docker container ls --all --filter "ancestor=hello" --format="{{.ID}}")
	docker image rm hello
